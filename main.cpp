#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <vector>
#include <map>
#include "main.h"

using namespace std;

int main(int argc, char *argv[])
{
	Cubes cubes;
	Tetris t;
	Cube cube;
	pair<int, Direction> next_pos;
	int elim_lines, combo = 0;

	initTables();

	if (argc == 2)
		cubes = initCubes(argv[1]);
	else
		cubes = initCubes();

	Region region = initRegion();
	while (!cubes.empty())
	{
		cube = cubes.back();
		cubes.pop_back();

		next_pos = t.nextPos(cube, region);
		if (!updateRegion(next_pos.first, next_pos.second, cube, region, t))
		{ // fail
			break;
		}

		elim_lines = updateElims(region);
		if (elim_lines)
		{
			combo++;
			updateScore(elim_lines, combo);
		}
		else
		{
			combo = 0;
		}

#if SHOW
		t.renderRegion(region);
#endif
	}

	game_over = true;
	updateScore();
	cout << "Final score: " << score << '\n';

	return 0;
}

void initTables()
{
	Ztable = {
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{1, 1}, Position{2, 1}},
		vector<Position>{Position{0, 1}, Position{0, 2}, Position{1, 0}, Position{1, 1}},
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{1, 1}, Position{2, 1}},
		vector<Position>{Position{0, 1}, Position{0, 2}, Position{1, 0}, Position{1, 1}},
	};

	Ltable = {
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{0, 2}, Position{1, 2}},
		vector<Position>{Position{0, 1}, Position{1, 1}, Position{2, 0}, Position{2, 1}},
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{1, 1}, Position{1, 2}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 0}, Position{2, 0}},
	};

	Qtable = {
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 0}, Position{1, 1}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 0}, Position{1, 1}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 0}, Position{1, 1}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 0}, Position{1, 1}},
	};

	Stable = {
		vector<Position>{Position{0, 1}, Position{1, 0}, Position{1, 1}, Position{2, 0}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 1}, Position{1, 2}},
		vector<Position>{Position{0, 1}, Position{1, 0}, Position{1, 1}, Position{2, 0}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 1}, Position{1, 2}},
	};

	Itable = {
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{0, 2}, Position{0, 3}},
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{2, 0}, Position{3, 0}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{0, 2}, Position{0, 3}},
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{2, 0}, Position{3, 0}},
	};

	Jtable = {
		vector<Position>{Position{0, 2}, Position{1, 0}, Position{1, 1}, Position{1, 2}},
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{2, 0}, Position{2, 1}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{0, 2}, Position{1, 0}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{1, 1}, Position{2, 1}},
	};

	Ttable = {
		vector<Position>{Position{0, 0}, Position{1, 0}, Position{1, 1}, Position{2, 0}},
		vector<Position>{Position{0, 0}, Position{0, 1}, Position{0, 2}, Position{1, 1}},
		vector<Position>{Position{0, 1}, Position{1, 0}, Position{1, 1}, Position{2, 1}},
		vector<Position>{Position{0, 1}, Position{1, 0}, Position{1, 1}, Position{1, 2}},
	};
}

/**
Example: cubes.txt
0, 1, 0, 1, 3, 4, 2, 1
**/
Cubes initCubes(char *file)
{
	string s;
	Cubes cubes;

	if (file != nullptr)
	{
		string strfile(file);
		string delim = ", ";
		ifstream cube_file(strfile);

		getline(cube_file, s); // one line
		cube_file.close();

		auto start = 0;
		auto end = s.find(delim);
		cubes_num = 0;

		while (end != string::npos)
		{
			cubes.push_back(
				static_cast<Cube>(stoi(s.substr(start, end - start)) % CUBE_NUM));
			cubes_num++;

			start = end + delim.length();
			end = s.find(delim, start);
		}

		if (cubes_num)
		{
			cubes.push_back(
				// last member no ', '
				static_cast<Cube>(stoi(s.substr(start, s.length())) % CUBE_NUM));
			cubes_num++;
		}
	}
	else
	{
		srand(time(NULL));
		for (int i = 0; i < cubes_num; i++)
#if DEBUG
			cubes.push_back(Icube);
#else
			cubes.push_back(static_cast<Cube>(rand() % CUBE_NUM));
#endif
	}

	return cubes;
}

Region initRegion()
{
	return Region(
		HEIGHT,				  // height
		vector<int>(WIDTH, 0) // width
	);
}

void updateScore(int elim_lines, int combos)
{
	if (game_over)
	{
		score += used_cube_num * SCORE_UNIT;
		return;
	}

	int elim_score = SCORE_UNIT * floor(pow(1.6, elim_lines));
	int combo_score = elim_score * floor(pow(1.3, combos));

	score += combo_score;
}

bool updateRegion(int x, Direction dir, Cube cube, Region &region, Tetris t)
{
	// get update position list
	bool collision = false;
	bool first = true;
	int y = 0;
	vector<Position> positions = getUpdatePosList(x, y, dir, cube);

	// lose game when overlap with other cube at beginning
	for (size_t i = 0; i < positions.size(); i++)
	{
		if (region[positions[i].second][positions[i].first]) // overlap with other cube
			return false;
	}

	while (!collision && y < HEIGHT)
	{
		if (first)
			first = false;
		else
			positions = getUpdatePosList(x, y, dir, cube);

#if SHOW
		t.emulateDown(region, positions);
#endif

		for (size_t i = 0; i < positions.size(); i++)
		{
			// valid region
			if (positions[i].first >= 0 && positions[i].first < WIDTH &&
				positions[i].second >= 0 && positions[i].second < HEIGHT)
			{

				// next y coor has cube
				if (positions[i].second + 1 == HEIGHT)
				{ // at bottom
					collision = true;
					break;
				}
				else if (region[positions[i].second + 1][positions[i].first])
				{
					collision = true;
					break;
				}
			}
			else
			{
				// out of region
				return false;
			}
		}

		if (collision)
		{
			// update cube
			for (size_t i = 0; i < positions.size(); i++)
			{
				region[positions[i].second][positions[i].first] = 1;
			}
			break;
		}

		y++;
	}
	used_cube_num++;

	return true;
}

vector<Position> getUpdatePosList(int x, int y, Direction dir, Cube cube)
{
	vector<Position> v;

	if (cube == Icube)
	{
		for (size_t i = 0; i < Itable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Itable[dir][i].first,
				y + Itable[dir][i].second));
		}
	}
	else if (cube == Jcube)
	{
		for (size_t i = 0; i < Jtable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Jtable[dir][i].first,
				y + Jtable[dir][i].second));
		}
	}
	else if (cube == Lcube)
	{
		for (size_t i = 0; i < Ltable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Ltable[dir][i].first,
				y + Ltable[dir][i].second));
		}
	}
	else if (cube == Qcube)
	{
		for (size_t i = 0; i < Qtable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Qtable[dir][i].first,
				y + Qtable[dir][i].second));
		}
	}
	else if (cube == Scube)
	{
		for (size_t i = 0; i < Stable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Stable[dir][i].first,
				y + Stable[dir][i].second));
		}
	}
	else if (cube == Tcube)
	{
		for (size_t i = 0; i < Ttable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Ttable[dir][i].first,
				y + Ttable[dir][i].second));
		}
	}
	else if (cube == Zcube)
	{
		for (size_t i = 0; i < Ztable[dir].size(); i++)
		{
			v.push_back(pair<int, int>(
				x + Ztable[dir][i].first,
				y + Ztable[dir][i].second));
		}
	}

	return v;
}

int updateElims(Region &region)
{
	int sum, i, lines = 0;

	for (i = 0; i < HEIGHT; i++)
	{
		sum = 0;
		for (int j = 0; j < WIDTH; j++)
		{
			if (region[i][j])
				sum++;
		}

		if (sum == WIDTH)
		{
			lines++;
			region.erase(region.begin() + i);
			region.insert(region.begin(), vector<int>(WIDTH, 0));
		}
	}

	return lines;
}
