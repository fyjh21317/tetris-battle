#ifndef TYPES_H
#define TYPES_H

#include <vector>
#include <utility>

enum Cube {Icube, Jcube, Lcube, Qcube, Scube, Tcube, Zcube, CUBE_NUM};
enum Direction {East, North, West, South, DIRECTION_NUM};

using namespace std;
using Region = vector<vector<int>>;
using Position = pair<int, int>;
using Cubes = vector<Cube>;

#endif
