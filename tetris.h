#ifndef TETRIS_H
#define TETRIS_H

#include <utility>
#include "types.h"
#define WAITTING_TIME 10000

class Tetris {
public:
    pair<int, Direction> nextPos(Cube cube, Region region);
    void renderRegion(Region region);
    void emulateDown(Region region, vector<Position> positions);
};

#endif

