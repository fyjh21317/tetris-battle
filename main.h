#ifndef MAIN_H
#define MAIN_H

#include "types.h"
#include "tetris.h"
#define SCORE_UNIT 10
#define HEIGHT 50
#define WIDTH 25
#define DEBUG 1
#define SHOW 1

Cubes initCubes(char *file=NULL);
Region initRegion();
void updateScore(int elim_lines = 0, int combos = 0);
void initTables();
vector<Position> getUpdatePosList(int x, int y, Direction dir, Cube cube);
bool updateRegion(int x, Direction dir, Cube cube, Region &region, Tetris t);
int updateElims(Region &region);

static unsigned int score = 0;
static unsigned int used_cube_num = 0;
static unsigned int cubes_num = 50;
static bool game_over = false;
static vector<vector<Position>> Ztable;
static vector<vector<Position>> Ltable;
static vector<vector<Position>> Qtable;
static vector<vector<Position>> Stable;
static vector<vector<Position>> Itable;
static vector<vector<Position>> Jtable;
static vector<vector<Position>> Ttable;

#endif
