#include <iostream>
#include <utility>
#include <unistd.h>
#include <math.h>
#include "main.h"
#include "tetris.h"
#include "types.h"

// https://stackoverflow.com/questions/55672661/what-this-character-sequence-033h-033j-does-in-c
#define clear() system("cls")

using namespace std;

#if DEBUG
static int owo = 0;
#endif
pair<int, Direction> Tetris::nextPos(Cube cube, Region region)
{
#if DEBUG
    int x = owo++ % WIDTH;
    Direction d = East;
#else
    // implement this function
    int x = rand() % WIDTH;
    Direction d = static_cast<Direction>(rand() % DIRECTION_NUM);
#endif

    return make_pair(x, d);
}

void Tetris::renderRegion(Region region)
{
    for (size_t i = 0; i < (region.size() / 2) + 2; i++)
        cout << '_';

    cout << '\n';
    for (size_t i = 0; i < region.size(); i++)
    {
        cout << '|';
        for (size_t j = 0; j < region[i].size(); j++)
        {
            if (region[i][j] == 0)
            {
                cout << ' ';
            }
            else
            {
                cout << '@';
            }
        }
        cout << "|\n";
    }

    for (size_t i = 0; i < (region.size() / 2) + 2; i++)
        cout << '_';

    cout << '\n';
}

void Tetris::emulateDown(Region region, vector<Position> positions)
{
    for (int i = 0; i < positions.size(); i++)
    {
        region[positions[i].second][positions[i].first] = 1;
    }
    clear();
    renderRegion(region);
    usleep(WAITTING_TIME);
}
