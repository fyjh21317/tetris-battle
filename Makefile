CC := g++
CFLAGS := -Wall -g -std=c++11

all:
	$(CC) $(CFLAGS) -o main tetris.cpp main.cpp

clean:
	rm main